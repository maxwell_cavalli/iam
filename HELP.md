# IAM Microservice
Microservice responsible to create / authenticate user

## Available API
### Authenticate User
* /api/auth/signin

### Create User
* /api/auth/signup

### Create User
* /api/auth/refreshtoken

