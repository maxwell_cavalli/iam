package br.com.easytax.authentication.services;

import br.com.easytax.authentication.domain.User;
import br.com.easytax.authentication.repository.UserRepository;
import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.iamlib.security.SecurityHelper;
import br.com.easytax.iamlib.service.BaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService extends BaseService<User> {

    private final PasswordEncoder encoder;
    private final RealmService realmService;
    private final ProfileService profileService;

    public UserService(final PasswordEncoder encoder,
                       final RealmService realmService,
                       final ProfileService profileService) {
        this.encoder = encoder;
        this.realmService = realmService;
        this.profileService = profileService;
    }

    public Optional<User> findByUsername(final String username) {
        return ((UserRepository) repository).findByUsername(username);
    }

    public boolean existsByUsername(final String username) {
        return ((UserRepository) repository).existsByUsername(username);
    }

    @Override
    public User beforeUpdate(final User previous, final User current) {
        final User userUpdated = super.beforeUpdate(previous, current);
        userUpdated.setUsername(previous.getUsername());
        userUpdated.setRealmId(previous.getRealmId());

        if (StringUtils.isNotBlank(userUpdated.getPassword())) {
            userUpdated.setPassword(encoder.encode(userUpdated.getPassword()));
        } else {
            userUpdated.setPassword(previous.getPassword());
        }

        if (userUpdated.getProfiles() == null) {
            userUpdated.setProfiles(previous.getProfiles());
        } else {
            userUpdated.getProfiles().forEach(s ->
                    profileService.get(s)
                            .orElseThrow(() -> {
                                throw new BadRequestException(String.format("Profile %s not found", s));
                            }));
        }

        return userUpdated;
    }

    @Override
    public User beforeCreate(User user) {
        final User userCreated = super.beforeCreate(user);
        if (!SecurityHelper.ROOT_REALM.equals(userCreated.getRealmId())) {
            realmService.get(userCreated.getRealmId())
                    .orElseThrow(() -> {
                        throw new BadRequestException("Invalid realmId");
                    });
        }

        return userCreated;
    }
}
