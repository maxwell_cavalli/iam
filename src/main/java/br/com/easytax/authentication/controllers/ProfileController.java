package br.com.easytax.authentication.controllers;

import br.com.easytax.authentication.domain.Profile;
import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.SecuredContext;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/profile")
@SecuredContext(context = "Profile")
public class ProfileController extends CrudApi<Profile> {

}
