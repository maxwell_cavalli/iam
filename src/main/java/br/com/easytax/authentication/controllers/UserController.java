package br.com.easytax.authentication.controllers;

import br.com.easytax.authentication.domain.User;
import br.com.easytax.authentication.services.UserService;
import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.Privilege;
import br.com.easytax.iamlib.security.SecuredContext;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/api/user")
@SecuredContext(context = "User")
public class UserController extends CrudApi<User> {

    private final UserService userService;
    private final PasswordEncoder encoder;

    @Privilege(actions = {"read"})
    @GetMapping("/retrieveByUserName/{username}")
    public ResponseEntity<?> retrieveUser(@PathVariable String username) {
        return ResponseEntity
                .ok(userService.findByUsername(username));
    }

    @Override
    public ResponseEntity<?> create(@Valid User user, HttpServletRequest request) {
        return ResponseEntity.badRequest().body("Not Allowed");
    }

    @Override
    public ResponseEntity<?> delete(String id) {
        return ResponseEntity.badRequest().body("Not Allowed");
    }

}
