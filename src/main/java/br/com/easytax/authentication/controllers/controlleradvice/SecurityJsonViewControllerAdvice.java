package br.com.easytax.authentication.controllers.controlleradvice;

import br.com.easytax.authentication.controllers.jsonview.Views;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.util.matcher.IpAddressMatcher;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

import java.util.List;

@RestControllerAdvice
class SecurityJsonViewControllerAdvice extends AbstractMappingJacksonResponseBodyAdvice {

    @Value("${core.whitelist.ip}")
    private List<String> whiteListIps;

    @Override
    protected void beforeBodyWriteInternal(
            MappingJacksonValue bodyContainer, MediaType contentType, MethodParameter returnType,
            ServerHttpRequest request, ServerHttpResponse response) {

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication instanceof AnonymousAuthenticationToken) {
                final String remoteAddress = ((WebAuthenticationDetails) authentication.getDetails()).getRemoteAddress();
                boolean hasAccess = whiteListIps.stream().anyMatch(strings -> {
                    IpAddressMatcher ipAddressMatcher = new IpAddressMatcher(strings);
                    return ipAddressMatcher.matches(remoteAddress);
                });

                if (hasAccess) {
                    bodyContainer.setSerializationView(Views.Internal.class);
                    return;
                }
            }

            bodyContainer.setSerializationView(Views.User.class);
        }

    }
}