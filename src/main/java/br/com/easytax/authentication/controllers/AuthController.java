package br.com.easytax.authentication.controllers;

import br.com.easytax.authentication.domain.RefreshToken;
import br.com.easytax.authentication.domain.User;
import br.com.easytax.authentication.exception.TokenRefreshException;
import br.com.easytax.authentication.payload.request.LogOutRequest;
import br.com.easytax.authentication.payload.request.LoginRequest;
import br.com.easytax.authentication.payload.request.SignupRequest;
import br.com.easytax.authentication.payload.request.TokenRefreshRequest;
import br.com.easytax.authentication.payload.response.JwtResponse;
import br.com.easytax.authentication.payload.response.MessageResponse;
import br.com.easytax.authentication.payload.response.TokenRefreshResponse;
import br.com.easytax.authentication.services.ProfileService;
import br.com.easytax.authentication.services.RefreshTokenService;
import br.com.easytax.authentication.services.UserService;
import br.com.easytax.iamlib.security.Privilege;
import br.com.easytax.iamlib.security.SecuredContext;
import br.com.easytax.iamlib.service.security.UserDetailsImpl;
import br.com.easytax.iamlib.service.security.jwt.JwtUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@SecuredContext(context = "Authentication")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    private final RefreshTokenService refreshTokenService;
    private final UserService userService;
    private final ProfileService profileService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody final LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        final String jwt = jwtUtils.generateJwtToken(userDetails);

        final List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        final RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
        return ResponseEntity
                .ok(new JwtResponse(jwt, refreshToken.getToken(), userDetails.getId(),
                        userDetails.getUsername(), userDetails.getRealmId(), roles));
    }

    @Privilege(actions = {"post"})
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userService.existsByUsername(signUpRequest.getUsername())){
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // Create new user's account
        final User user = new User(signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getRealmId());

        user.setProfiles(
                signUpRequest.getRole().stream()
                        .map(String::toUpperCase)
                        .collect(Collectors.toSet())
        );

        return userService.create(user)
                .map(t1 -> ResponseEntity.ok(new MessageResponse("User registered successfully!")))
                .orElse(ResponseEntity.badRequest().build());
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    final Set<String> roles = new HashSet<>();

                    user.getProfiles().forEach(s -> profileService.get(s).ifPresent(profile -> roles.addAll(profile.getRoles())));

                    String token = jwtUtils.generateTokenFromUsername(UserDetailsImpl.build(user, roles));
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logoutUser(@Valid @RequestBody LogOutRequest logOutRequest) {
        refreshTokenService.deleteByUserId(logOutRequest.getUserId());
        return ResponseEntity.ok(new MessageResponse("Log out successful!"));
    }

}
