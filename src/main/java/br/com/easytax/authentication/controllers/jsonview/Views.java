package br.com.easytax.authentication.controllers.jsonview;

public interface Views {
    interface User {
    }

    interface AuthenticatedUser extends User {
    }

    interface Internal extends AuthenticatedUser {
    }
}