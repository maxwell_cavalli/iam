package br.com.easytax.authentication.controllers;

import br.com.easytax.authentication.domain.Realm;
import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.SecuredContext;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/realm")
@SecuredContext(context = "Realm")
public class RealmController extends CrudApi<Realm> {


}
