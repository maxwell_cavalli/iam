package br.com.easytax.authentication.repository;

import br.com.easytax.authentication.domain.User;
import br.com.easytax.iamlib.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends BaseRepository<User> {

    Optional<User> findByUsername(final String username);

    Boolean existsByUsername(final String username);
}
