package br.com.easytax.authentication.repository;

import br.com.easytax.authentication.domain.RefreshToken;
import br.com.easytax.authentication.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends MongoRepository<RefreshToken, String> {

  Optional<RefreshToken> findByToken(final String token);

  int deleteByUser(final User user);
}
