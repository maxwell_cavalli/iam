package br.com.easytax.authentication.repository;

import br.com.easytax.authentication.domain.Profile;
import br.com.easytax.iamlib.repository.BaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProfileRepository extends BaseRepository<Profile> {

}
