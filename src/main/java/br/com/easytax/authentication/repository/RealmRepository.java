package br.com.easytax.authentication.repository;

import br.com.easytax.authentication.domain.Realm;
import br.com.easytax.iamlib.repository.BaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RealmRepository extends BaseRepository<Realm> {

}
