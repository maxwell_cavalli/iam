package br.com.easytax.authentication.payload.response;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String refreshToken;
	private String id;
	private String username;
	private List<String> roles;
	private String realmId;

	public JwtResponse(final String accessToken,
					   final String refreshToken,
					   final String id,
					   final String username,
					   final String realmId,
					   final List<String> roles) {
		this.token = accessToken;
		this.refreshToken = refreshToken;
		this.id = id;
		this.username = username;
		this.realmId = realmId;
		this.roles = roles;
	}


}
