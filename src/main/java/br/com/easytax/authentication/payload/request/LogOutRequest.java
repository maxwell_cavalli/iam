package br.com.easytax.authentication.payload.request;

import lombok.Data;

@Data
public class LogOutRequest {

  private String userId;

}
