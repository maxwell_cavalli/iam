package br.com.easytax.authentication.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Document("realms")
@CompoundIndex(def = "{'name': 1}")
public class Realm extends BaseDomain {

    @NotBlank
    @Size(max = 20)
    private String name;

    @NotBlank
    @Size(max = 20)
    private String path;

}
