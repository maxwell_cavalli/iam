package br.com.easytax.authentication.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@Document("profiles")
@CompoundIndex(def = "{'name': 1}")
public class Profile extends BaseDomain {

    @NotBlank
    @Size(max = 20)
    private String name;

    private Set<String> roles = new HashSet<>();



}
