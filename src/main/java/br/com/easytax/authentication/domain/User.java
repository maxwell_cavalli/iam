package br.com.easytax.authentication.domain;

import br.com.easytax.authentication.controllers.jsonview.Views;
import br.com.easytax.iamlib.domain.BaseDomain;
import br.com.easytax.iamlib.domain.IUser;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@Document("users")
@CompoundIndex(def = "{'username': 1}")
public class User extends BaseDomain implements IUser {

    @NotBlank
    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 120)
    @JsonView(Views.Internal.class)
    private String password;

    @NotBlank
    private String realmId;

    private Set<String> profiles = new HashSet<>();

    public User(final String username, final String password, final String realmId) {
        this.username = username;
        this.password = password;
        this.realmId = realmId;
    }

}
