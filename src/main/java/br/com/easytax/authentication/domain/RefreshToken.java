package br.com.easytax.authentication.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document("refreshtoken")
@CompoundIndex(def = "{'user': 1, 'token': 1}", unique = true)
public class RefreshToken extends BaseDomain {

    private User user;

    private String token;

    private Instant expiryDate;

}
