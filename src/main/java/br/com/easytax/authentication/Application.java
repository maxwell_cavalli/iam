package br.com.easytax.authentication;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@SpringBootApplication
@ComponentScan(basePackages =
		{
				"br.com.easytax.iamlib.service.authorization",
				"br.com.easytax.iamlib.service.security",
				"br.com.easytax.iamlib.security",
				"br.com.easytax.iamlib.controller.exception",
				"br.com.easytax.authentication"
		})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


	@Bean
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder mapperBuilder) {
		return mapperBuilder.build().setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

}
